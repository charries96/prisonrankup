package net.craftservers.prisonrankup.Models;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class SubCommand {

    public SubCommand(String name) {
        CommandHandler.registerSubCommand(name, this);
    }

    public abstract void onCommand(CommandSender sender, Command cmd, String label, String[] args);

    public abstract void onCommand(Player player, Command cmd, String label, String[] args);

}
