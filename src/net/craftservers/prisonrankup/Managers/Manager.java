package net.craftservers.prisonrankup.Managers;

public class Manager {

    private static ConfigManager config = new ConfigManager();
    private static RankManager rm = new RankManager();

    public static ConfigManager getConfigManager() {
        return config;
    }

    public static RankManager getRankManager() {return rm;}

}
