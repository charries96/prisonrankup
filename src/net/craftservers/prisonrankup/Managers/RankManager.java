package net.craftservers.prisonrankup.Managers;

import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;

public class RankManager {

    FileConfiguration config = ConfigManager.getConfig();

    public List<String> getAllRanks() {
        List<String> ls = new ArrayList<>();
        for(String s : config.getStringList("groups")) {
            String[] data = s.split(":");
            ls.add(data[0]);
        }
        return ls;
    }

    public void createRank(String name, double price) {
        List<String> ls = config.getStringList("groups");
        ls.add(name + ":" + price);
        config.set("groups", ls);
        ConfigManager.saveConfig();
    }

    public void createRank(String name, double price, int index) {
        List<String> ls = config.getStringList("groups");
        ls.add(index, name + ":" + price);
        config.set("groups", ls);
        ConfigManager.saveConfig();
    }

    public void deleteRank(String name) {
        List<String> ls = config.getStringList("groups");
        ls.remove(name);
        config.set("groups", ls);
        ConfigManager.saveConfig();
    }

    public void deleteRank(int index) {
        List<String> ls = config.getStringList("groups");
        ls.remove(index);
        config.set("groups", ls);
        ConfigManager.saveConfig();
    }

    public void clearRanks() {
        List<String> exampleList = new ArrayList<>();
        exampleList.add("A:100");
        exampleList.add("B:350");
        exampleList.add("C:450");
        config.set("groups", exampleList);
        ConfigManager.saveConfig();
    }

    public int getLocation(String rankName) {
        int location = 0;
        for(String s : config.getStringList("groups")) {
            String[] data = s.split(":");
            if(rankName.equalsIgnoreCase(data[0])) {
                break;
            }
            location++;
        }
        return location;
    }

    public String getDataByName(String name) {
        String data = "";
        for(String s : config.getStringList("groups")) {
            if(s.split(":")[0].equalsIgnoreCase(name)) {
                data = s;
                break;
            }
        }
        return data;
    }

}
