package net.craftservers.prisonrankup.Utils.Events;

import net.craftservers.prisonrankup.Models.PRPlayer;
import net.craftservers.prisonrankup.Models.Rank;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by mazen on 5/30/14.
 */
public class PlayerRankupEvent extends Event{

    private static final HandlerList handlers = new HandlerList();
    private PRPlayer player;
    private Rank oldRank;

    public PlayerRankupEvent(PRPlayer player, Rank oldRank) {
        this.player = player;
        this.oldRank = oldRank;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public PRPlayer getPlayer() {return player;}

    public Rank getOldRank() {return oldRank;}

    public Rank getNewRank() {return getPlayer().getCurrentRank();}
}
