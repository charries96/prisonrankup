package net.craftservers.prisonrankup.Listeners;

import net.craftservers.prisonrankup.Managers.Manager;
import net.craftservers.prisonrankup.Models.PRPlayer;
import net.craftservers.prisonrankup.Models.Rank;
import net.craftservers.prisonrankup.Utils.Events.GroupAddEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class ConfigUpdater implements Listener {

    @EventHandler
    public void onGroupAdd(GroupAddEvent event) {
        if(Manager.getRankManager().getAllRanks().contains(event.getNewGroup())) {
            PRPlayer player = new PRPlayer(event.getTarget().getName());
            player.setRank(new Rank(event.getNewGroup()));
        }
    }
}
